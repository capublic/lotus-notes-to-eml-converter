package com.canda.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class ConvertToEmlFiles {

	private static class Mail {
		private int number;

		/**
		 *
		 */
		public Mail(int aNumber) {
			number = aNumber;
		}

		StringBuilder content = new StringBuilder();
		String subject;
	}

	private static Map<String, Integer> sameNameCounts = new HashMap<>();

	/**
	 * Drag and Drop limit
	 */
	private static int groupSize = 1000;

	public static void main(String[] args) throws IOException {
		if (args == null || args.length == 0) {
			System.out.println();
			System.out.println("Parameter 1 missing: AbsoutePathToNotesExport.txt");
			System.out.println("mvn compile exec:java \"-Dexec.args=\'C:\\AbsolutePath\\ToThe\\TextFile.txt'\"");
			System.out.println("Parameter 2 optional: TargetDirectory");
			System.out.println("mvn compile exec:java \"-Dexec.args=\'C:\\AbsolutePath\\ToThe\\TextFile.txt' 'C:\\temp'\"");
			System.exit(1);
			return;
		}
		File tempSource = new File(args[0]).toPath().normalize().toFile();
		if (!tempSource.exists()) {
			System.out.println();
			System.out.println("File " + tempSource.getAbsolutePath() + " is not existing.");
			System.exit(2);
			return;
		}
		System.out.println("Read from " + tempSource.getAbsolutePath());
		File tempTargetDir;
		String tempRootDir;
		if (args.length == 2) {
			tempRootDir = args[1];
		} else {
			tempRootDir = System.getProperty("java.io.tmpdir");
		}
		tempTargetDir = new File(tempRootDir + File.separator + tempSource.getName() + ".emls").toPath().normalize().toFile();
		System.out.println("Target dir: " + tempTargetDir.getAbsolutePath() + File.separator);
		tempTargetDir.mkdirs();
		if (!tempTargetDir.isDirectory()) {
			System.out.println();
			System.out.println("Target " + tempTargetDir.getAbsolutePath() + " is not a Directory.");
			System.exit(3);
			return;
		}
		File[] tempListCurrentTempFiles = tempTargetDir.listFiles();
		if (tempListCurrentTempFiles != null && tempListCurrentTempFiles.length > 0) {
			System.out.println("Delete previously created files out of " + tempTargetDir);
			for (File tempFile : tempListCurrentTempFiles) {
				if (tempFile.isDirectory()) {
					System.out.println("Del subdir: " + tempFile);
					File[] tempSubDirContent = tempFile.listFiles();
					for (File tempSubDir : tempSubDirContent) {
						tempSubDir.delete();
					}
				}
				tempFile.delete();
			}
		}
		System.out.println("Start reading " + tempSource + "...");
		int i = 1;
		int dirNumber = 0;
		File tempTargetSubDir = null;
		try (BufferedReader tempIn = new BufferedReader(new FileReader(tempSource))) {
			Mail tempMail = new Mail(i);
			String tempLine = tempIn.readLine();
			while (tempLine != null) {
				if (tempLine.startsWith("")) {
					if ((i % groupSize) == 1) {
						String tempSubDirNumber = new DecimalFormat("0000").format(dirNumber);
						tempTargetSubDir = new File(tempTargetDir.getAbsolutePath() + File.separator + tempSubDirNumber);
						tempTargetSubDir.mkdir();
						dirNumber += 1;
					}
					finishMail(tempTargetSubDir, tempMail);
					i++;
					tempMail = new Mail(i);
				} else {
					if (tempLine.startsWith("Subject: ")) {
						tempMail.subject = tempLine.substring("Subject: ".length()).trim();
						tempMail.content.append(tempLine + "\r\n");
					} else if (tempLine.startsWith("SendTo:")) {
						tempMail.content.append("To:" + tempLine.substring("SendTo:".length()) + "\r\n");
					} else if (tempLine.startsWith("COPYTO:")) {
						tempMail.content.append("Cc:" + tempLine.substring("COPYTO:".length()) + "\r\n");
					} else {
						tempMail.content.append(tempLine + "\r\n");
					}
				}
				tempLine = tempIn.readLine();
			}
			finishMail(tempTargetSubDir, tempMail);
		}
		System.out.println();
		System.out.println("Finished reading from " + tempSource.getAbsolutePath());
		System.out.println("Result dir: " + tempTargetDir.getAbsolutePath());
		System.out.println("Result dir contains " + i + " .eml files now.");
		System.out.println();
	}

	/**
	 * @throws IOException
	 *
	 */
	private static void finishMail(File tempDir, Mail aMail) throws IOException {
		String tempFileName;
		String tempContentString = aMail.content.toString();
		if (tempContentString.trim().length() == 0) {
			return;
		}
		if (aMail.subject == null) {
			tempFileName = "mail-" + aMail.number + ".eml";
		} else {
			String tempValidName = aMail.subject;
			tempValidName = tempValidName.replace('*', '_');
			tempValidName = tempValidName.replace('?', '_');
			tempValidName = tempValidName.replace('\"', '_');
			tempValidName = tempValidName.replace('\'', '_');
			tempValidName = tempValidName.replace('|', '_');
			tempValidName = tempValidName.replace('>', '_');
			tempValidName = tempValidName.replace('<', '_');
			tempValidName = tempValidName.replace(' ', '_');
			tempValidName = tempValidName.replace('\\', '_');
			tempValidName = tempValidName.replace('/', '_');
			tempValidName = tempValidName.replace(':', '_');
			tempValidName = tempValidName.replace(';', '_');
			tempValidName = tempValidName.replace(',', '.');
			if (tempValidName.length() > 100) {
				tempValidName = tempValidName.substring(0, 100);
			}
			String tempName = "";
			for (int i = 0; i < tempValidName.length(); i++) {
				char c = tempValidName.charAt(i);
				if (32 < c && c < 126) {
					tempName += c;
				} else {
					tempName += "_";
				}
			}
			Integer tempSameNameCount = sameNameCounts.get(tempName);
			if (tempSameNameCount == null) {
				tempSameNameCount = 1;
			} else {
				tempSameNameCount += 1;
			}
			sameNameCounts.put(tempName, tempSameNameCount);
			if (tempSameNameCount > 1) {
				tempName += "(" + tempSameNameCount + ")";
			}
			tempFileName = tempName + ".eml";
		}
		System.out.println(aMail.number + " " + tempFileName);
		String tempAbsoluteFileName = tempDir + File.separator + tempFileName;
		try (FileWriter tempOut = new FileWriter(tempAbsoluteFileName)) {
			tempOut.write(tempContentString);
		}
	}
}
